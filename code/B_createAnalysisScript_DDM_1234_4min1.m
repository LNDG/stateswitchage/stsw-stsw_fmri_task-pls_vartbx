% This script extracts the necessary infos for a PLS analysis of
% dimensionality. Unfortunately, text files are hard to edit, so I rely on
% manual editing in the end. The necessary information is output in the
% command window and can be directly copied to replace the placeholders.

clear all; clc;

%% get DDM parameters from MRI session

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
dataFileMRI = [pn.root, 'B_data/MRI/MRI_QuantileOpt.csv'];
tableDataMRI = readtable(dataFileMRI, 'ReadRowNames', 1);
arrayDataMRI = table2array(tableDataMRI);
ColumnNamesMRI = tableDataMRI.Properties.VariableNames'; clear tableDataMRI;

% get individual subject data

params = {'a'; 't'; 'v'};
conditions = {'1'; '2'; '3'; '4'};

IndicesMRI = []; MeanValuesMRI = [];
for indParam = 1:numel(params)
    for indCond = 1:numel(conditions)
        strPattern=[params{indParam}, '_',conditions{indCond},'_'];
        IndicesMRI = find(contains(ColumnNamesMRI,strPattern));
        MeanValuesMRI(indParam,indCond,:) = arrayDataMRI(:,IndicesMRI);
    end
end

% get corresponding IDs

dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';
csvDataMRI = csvread([dataPath,'StateSwitchDynamicTrialData_MRI.dat'],2,0);
IDsMRI = cellstr(num2str(unique(csvDataMRI(:,7))));

%% YA:

clearvars -except MeanValuesMRI IDsMRI

% removed 1215
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

numSubs = numel(find(ismember(IDsMRI,IDs)));
for indCond = 1:5
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_VarTbx_v1_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(IDsMRI, IDs{indID}));
        if indCond <=4
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(MeanValuesMRI(1, indCond, curID),2); % threshold a
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(MeanValuesMRI(2, indCond, curID),2); % non-decision time t
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(MeanValuesMRI(3, indCond, curID),2); % drift rate v
        elseif indCond == 5
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(MeanValuesMRI(1, 2:4, curID),2)-nanmean(MeanValuesMRI(1, 1, curID),2); % threshold a
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(MeanValuesMRI(2, 2:4, curID),2)-nanmean(MeanValuesMRI(2, 1, curID),2); % non-decision time t
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(MeanValuesMRI(3, 2:4, curID),2)-nanmean(MeanValuesMRI(3, 1, curID),2); % drift rate v
        end
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d \n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3});
end

%% OAs:

clearvars -except MeanValuesMRI IDsMRI

%removed 2258
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};

numSubs = numel(find(ismember(IDsMRI,IDs)));
for indCond = 1:5
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_VarTbx_v1_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(IDsMRI, IDs{indID}));
        if indCond <=4
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(MeanValuesMRI(1, indCond, curID),2); % threshold a
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(MeanValuesMRI(2, indCond, curID),2); % non-decision time t
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(MeanValuesMRI(3, indCond, curID),2); % drift rate v
        elseif indCond == 5
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(MeanValuesMRI(1, 2:4, curID),2)-nanmean(MeanValuesMRI(1, 1, curID),2); % threshold a
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(MeanValuesMRI(2, 2:4, curID),2)-nanmean(MeanValuesMRI(2, 1, curID),2); % non-decision time t
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(MeanValuesMRI(3, 2:4, curID),2)-nanmean(MeanValuesMRI(3, 1, curID),2); % drift rate v
        end
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d \n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3});
end
