% Caluclate PLS relationship between dimensionality changes (4-1) and BOLD
% SD changes(4-1) [behavioral PLS]

% 180323 | adapted by JQK from Steffen's script

clear all; clc; restoredefaultpath;

pn.root      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	 = [pn.root, 'analyses/D2_PLS_VarTbx/T_tools/'];  addpath(genpath(pn.tools));
pn.data      = [pn.root, 'analyses/D2_PLS_VarTbx/B_data/'];
pn.matpath   = [pn.data, 'A_PLSdir/'];

% 1215, 2258 missing
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};

conditions = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

%% change the datamat information

for indID = 1:(numel(IDs))
    
    disp(['Processing subject ', IDs{indID}]);

    %% load _BfMRIsessiondata.mats variables to be changed 
    try
      a = load([pn.matpath, 'SD_', IDs{indID}, '_VarTbx_v1_BfMRIsessiondata.mat']);
    catch ME
      disp (ME.message)
    end

    a.session_info.datamat_prefix   = (['SD_', IDs{indID}, '_VarTbx_v1']);

    %% compute conditions 5 to 10
    % cond5 = cond4 - cond1
    a.st_datamat(5,:) = a.st_datamat(4,:)-a.st_datamat(1,:);
    % cond6 = cond34 - cond12
    a.st_datamat(6,:) = nanmean(a.st_datamat(3:4,:),1)-nanmean(a.st_datamat(1:2,:),1);
    % cond7 = cond234 - cond1
    a.st_datamat(7,:) = nanmean(a.st_datamat(2:4,:),1)-nanmean(a.st_datamat(1,:),1);
    % cond8 = 234
    a.st_datamat(8,:) = nanmean(a.st_datamat(2:4,:),1);
    % cond9 = 4 - 3
    a.st_datamat(9,:) = nanmean(a.st_datamat(4,:),1)-nanmean(a.st_datamat(3,:),1);
    % cond10 = 3 - 2
    a.st_datamat(10,:) = nanmean(a.st_datamat(3,:),1)-nanmean(a.st_datamat(2,:),1);

    % cond11 = cond1234
    a.st_datamat(11,:) = nanmean(a.st_datamat(1:4,:),1);
    % cond12 = cond234 - cond1
    a.st_datamat(12,:) = nanmean(a.st_datamat(2:4,:),1)-nanmean(a.st_datamat(1,:),1);
    % cond13 = cond1234
    a.st_datamat(13,:) = nanmean(a.st_datamat(1:4,:),1);
    % cond14 = cond234 - cond1
    a.st_datamat(14,:) = nanmean(a.st_datamat(2:4,:),1)-nanmean(a.st_datamat(1,:),1);

    % cond15 = cond2 - cond1
    a.st_datamat(15,:) = nanmean(a.st_datamat(2,:),1)-nanmean(a.st_datamat(1,:),1);
    % cond16 = cond3 - cond2
    a.st_datamat(16,:) = nanmean(a.st_datamat(3,:),1)-nanmean(a.st_datamat(2,:),1);
    % cond17 = cond4 - cond3
    a.st_datamat(17,:) = nanmean(a.st_datamat(4,:),1)-nanmean(a.st_datamat(3,:),1);

    % cond18 = cond4 residual (cond1)
    [~, ~, a.st_datamat(18,:)] = regress(a.st_datamat(4,:)', a.st_datamat(1,:)');

    % cond19 = cond4 - cond3
    a.st_datamat(19,:) = nanstd(a.st_datamat,[],1);

    % relChange 234 vs 1
    a.st_datamat(20,:) = (nanmean(a.st_datamat(2:4,:),1)-nanmean(a.st_datamat(1,:),1))./nanmean(a.st_datamat(1,:),1);

    % linear 1234 regression
    for indVox = 1:size(a.st_datamat,2)
       X = [1 1; 1 2; 1 3; 1 4];
       b = regress(a.st_datamat(1:4, indVox), X);
       a.st_datamat(21,indVox) = b(2);
    end
    
    %% edit conditions labels
    a.session_info.condition = cell(21,1);
    a.session_info.condition(1,1) = {'SDLoad1'};
    a.session_info.condition(2,1) = {'SDLoad2'};
    a.session_info.condition(3,1) = {'SDLoad3'};
    a.session_info.condition(4,1) = {'SDLoad4'};
    a.session_info.condition(5,1) = {'SDLoad4min1'};
    a.session_info.condition(6,1) = {'SDLoad34min12'};
    a.session_info.condition(7,1) = {'SDLoad234min1'};
    a.session_info.condition(8,1) = {'SDLoad234'};
    a.session_info.condition(9,1) = {'SDLoad4min3'};
    a.session_info.condition(10,1) = {'SDLoad3min2'};

    a.session_info.condition(11,1) = {'SDLoad1234'};
    a.session_info.condition(12,1) = {'SDLoad234min1'};
    a.session_info.condition(13,1) = {'SDLoad1234'};
    a.session_info.condition(14,1) = {'SDLoad234min1'};
    a.session_info.condition(15,1) = {'SDLoad2min1'};
    a.session_info.condition(16,1) = {'SDLoad3min2'};
    a.session_info.condition(17,1) = {'SDLoad4min3'};

    a.session_info.condition(18,1) = {'SDLoad4res1'};
    a.session_info.condition(19,1) = {'SDAcrossLoads'};
    a.session_info.condition(20,1) = {'SD234rel1'};
    a.session_info.condition(20,1) = {'SDlinear1234'};

    a.session_info.condition0 = a.session_info.condition;

    %% edit condition counters
    a.session_info.num_conditions  = numel(a.session_info.condition);
    a.session_info.num_conditions0 = numel(a.session_info.condition);

    %% add zero vector to st_datamat if condition(=session) not available
    % update condition variables according to count of conditions

    for indCond = size(a.session_info.condition_baseline,2)+1:numel(a.session_info.condition)

       a.session_info.condition_baseline{indCond}  = a.session_info.condition_baseline{1};
       a.session_info.condition_baseline0{indCond} = a.session_info.condition_baseline0{1};
    %        a.session_info.run(indCond).blk_onsets      = a.session_info.run(1).blk_onsets;
    %        a.session_info.run(indCond).blk_length      = a.session_info.run(1).blk_length;
    end

    % num_subj_cond AND st_evt_list: ones or numbers = count of conditions
    a.num_subj_cond = repmat(1,1,numel(a.session_info.condition));
    a.st_evt_list   = 1:1:numel(a.session_info.condition);

    %% save new data file
    save([pn.matpath, a.session_info.datamat_prefix '_BfMRIsessiondata.mat'], '-struct', 'a');

    clear a
   
end