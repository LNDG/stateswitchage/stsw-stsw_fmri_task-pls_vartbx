% Create info file for PLS analysis

% for ER-behavioral PLS: include AMF, relevant DDM parameters

clear all; clc;

%% get summary data

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary_YAOA.mat'], 'STSWD_summary')

%% YA:

% removed 1215
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

numSubs = numel(find(ismember(STSWD_summary.IDs,IDs)));
IDs = IDs(ismember(IDs,STSWD_summary.IDs));
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_VarTbx_v1_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        % add spectral EEG PLS
        dataForPLS{(indCond-1)*numSubs+indID,1} = STSWD_summary.EEGpupil.slope_win(curID);
        % add DDM results
        dataForPLS{(indCond-1)*numSubs+indID,2} = STSWD_summary.HDDM_vt.nondecisionMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,3} = STSWD_summary.HDDM_vt.nondecisionMRI_linear(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,4} = STSWD_summary.HDDM_vt.driftMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,5} = STSWD_summary.HDDM_vt.driftMRI_linear(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,6} = STSWD_summary.HDDM_vt.thresholdMRI(curID,1);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

% normalize each variable: convert to z-scores
%dataForPLS_z = [dataForPLS(:,1), num2cell(zscore(cell2mat(dataForPLS(:,2:end))))];

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, ...
        Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6});
end

clear dataForPLS Fillin

%% OA:
 
%removed 2258
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};

numSubs = numel(find(ismember(STSWD_summary.IDs,IDs)));
IDs = IDs(ismember(IDs,STSWD_summary.IDs));
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_VarTbx_v1_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        % add spectral EEG PLS
        dataForPLS{(indCond-1)*numSubs+indID,1} = STSWD_summary.EEGpupil.slope_win(curID);
        % add DDM results
        dataForPLS{(indCond-1)*numSubs+indID,2} = STSWD_summary.HDDM_vt.nondecisionMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,3} = STSWD_summary.HDDM_vt.nondecisionMRI_linear(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,4} = STSWD_summary.HDDM_vt.driftMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,5} = STSWD_summary.HDDM_vt.driftMRI_linear(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,6} = STSWD_summary.HDDM_vt.thresholdMRI(curID,1);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

% normalize each variable: convert to z-scores
%dataForPLS_z = [dataForPLS(:,1), num2cell(zscore(cell2mat(dataForPLS(:,2:end))))];

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, ...
        Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6});
end
