function S0A_commonGMvoxels_summary_VarTbx()
% Identify common GM coordinates to constrain analyses to.
%   nii_coords                    | non-NaN voxels across subjects
%   GM_coords                     | MNI GM mask voxels
%   final_coords                  | non-NaN MNI GM voxels
%   nii_nonZero                   | non-NaN, non-zero-power voxels across subjects
%   final_coords_withoutZero      | non-NaN, non-zero-power GM voxels across subjects

% PLS requires exlusively non-NaN voxels. GM voxels and non-Zero-power is
% optional, however may be recommended. Note that by focussing on
% non-Zero-power voxels, each subject contributes an identical amount of
% samples (N) to the analysis. Non-Zero-power voxels are usually located
% outside the brain and may inter-individually differ in number depending 
% on the coregistration with MNI. 

% Folder 'VoxelOverlap' has to be created manually and GM MNI mask has to
% be shifted into this directory.

% 171220 | adapted from SW, JR by JQK
% 180223 | adapted for STSWD
% 180322 | created this version, which uses the individual summary images
%           rather than loading the raw values.
% 180801 | adapted for StateSwitch task
% 190129 | adapted for VarTbx input

% Note: For '2131' and '2237' only the first two runs are available.

%% paths & setup

BASEPATH = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
NIIPATH = '/Users/kosciessa/Desktop/beegfs/StateSwitch/WIP/G_GLM/B_data/F_GLM1stLevelout_v5/';
DATAPATH = [BASEPATH, 'analyses/D2_PLS_VarTbx/B_data/B_VoxelOverlap/'];
addpath(genpath([BASEPATH,'analyses/B_PLS/T_tools/NIFTI_toolbox/']));
addpath(genpath([BASEPATH,'analyses/B_PLS/T_tools/preprocessing_tools/']));  
    
    % 1215, 2131, 2237, 2258 excluded for now
    IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
        '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
        '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
        '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
        '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
        '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
        '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
        '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};
        
%% get grey matter coordinates based on MNI GM mask

    GM_coords = load_nii([DATAPATH, 'GM_MNI3mm_mask.nii']); % JQK: mask manually copied from EyeMem directory 
    GM_coords = reshape(GM_coords.img, [],1);
    GM_coords = find(GM_coords);

%% identify non-NaN voxels & non-zero power voxels

    nii_coords=1:60*72*60 ; % 3mm
    nii_nonZero=nii_coords;

    for indID = 1:numel(IDs)
        for indCond = 1:4
            NIINAME=(['sd_', IDs{indID}, '__load', num2str(indCond), '_sd']);
            coords=load_nii([NIIPATH NIINAME '.nii']);
            coords=reshape(coords.img,[],coords.hdr.dime.dim(5));
            coords(1:100,:) = NaN;
            coords_noNaN = find(~isnan(coords));
            nii_coords=intersect(nii_coords,coords_noNaN);
            % find non-zero power voxels across subjects
            coords_nonZero = find(~isnan(coords) & coords~=0);
            nii_nonZero=intersect(nii_nonZero,coords_nonZero);
            clear coords
        end
        disp (['Done with ' IDs{indID}])
    end

%% indices of non-NaN GM voxels

    final_coords = intersect(nii_coords, GM_coords);                            % non-NaN GM voxels across subjects
    final_coords_withoutZero = intersect(nii_nonZero, GM_coords);               % non-NaN & non-zero GM voxels across subjects

%% save coordinate mat

    save([DATAPATH, 'coords_N',num2str(numel(IDs)),'.mat'] ,'nii_coords', 'final_coords', 'GM_coords', 'final_coords_withoutZero', 'nii_nonZero');

%% save niftis
    
    tempNii = load_nii([DATAPATH, 'GM_MNI3mm_mask.nii']);

    nii_coords=zeros(60,72,60) ; % 3mm
    nii_coords(final_coords_withoutZero) = 1;
    tempNii.img = nii_coords;
    save_nii(tempNii,[DATAPATH, 'coords_nozero_N',num2str(numel(IDs)),'.nii'])

    nii_coords=zeros(60,72,60) ; % 3mm
    nii_coords(final_coords) = 1;
    tempNii.img = nii_coords;
    save_nii(tempNii,[DATAPATH, 'coords_noNaN_N',num2str(numel(IDs)),'.nii'])
    
end
