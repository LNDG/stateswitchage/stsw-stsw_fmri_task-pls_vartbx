% create PLS input from Varibility toolbox matrices

% copy original PLS mean matrices
% add in information from Variability Toolbox

% 190129 | created by JQK

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D2_PLS_VarTbx/';
pn.VarTBXfiles = '/Users/kosciessa/Desktop/beegfs/StateSwitch/WIP/G_GLM/B_data/F_GLM1stLevelout_v5/';
pn.origFiles = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/';
pn.maskFile = [pn.root, 'B_data/B_VoxelOverlap/'];
pn.outPath = [pn.root, 'B_data/A_PLSdir/'];
pn.tools = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/T_tools/']; addpath(genpath(pn.tools));

% removed 2258 for now, as processing has not yet finished
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};

numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc

for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}, '.']);

    % load subject's sessiondata file
    a = load([pn.origFiles, 'task_', IDs{indID}, '_BfMRIsessiondata.mat']);

    % load common coordinates
    load([pn.maskFile, 'coords_N93.mat'], 'final_coords_withoutZero');
    final_coords = final_coords_withoutZero;
    
    conditions=a.session_info.condition(1:numConds_raw);
    a = rmfield(a,'st_datamat');
    a = rmfield(a,'st_coords');

    %replace fields with correct info.
    a.session_info.datamat_prefix   = (['SD_', IDs{indID}, '_VarTbx_v1']); % SD PLS file name; _Bfmirsessiondata will be automatically appended!
    a.st_coords = final_coords;     % constrain analysis to shared non-zero GM voxels
    a.pls_data_path = pn.outPath;

    % load subject values for corresponding coordinates
    for indCond = 1:numConds_raw
        fname = [pn.VarTBXfiles, 'sd_',IDs{indID},'__load',num2str(indCond),'_sd.nii'];
        img = double(S_load_nii_2d(fname)); clear fname;
        img = img(final_coords,:); % restrict to final_coords
        a.st_datamat(indCond,:) = img;
        clear img;
    end
    a.st_evt_list = a.st_evt_list(1:size(a.st_datamat,1));
    a.num_subj_cond = a.num_subj_cond(1:size(a.st_datamat,1));
    
    a.session_info.num_conditions = size(a.st_datamat,1);
    a.session_info.condition = a.session_info.condition(1:size(a.st_datamat,1));
    a.session_info.condition_baseline = a.session_info.condition_baseline(1:size(a.st_datamat,1));
    a.session_info.num_conditions0 = size(a.st_datamat,1);
    a.session_info.condition0 = a.session_info.condition0(1:size(a.st_datamat,1));
    a.session_info.condition_baseline0 = a.session_info.condition_baseline0(1:size(a.st_datamat,1));
    a.session_info.run = 1:4;
    
    save([pn.outPath, a.session_info.datamat_prefix ,'_BfMRIsessiondata.mat'],'-struct','a','-mat');
    disp ([IDs{indID} ' done!'])
end