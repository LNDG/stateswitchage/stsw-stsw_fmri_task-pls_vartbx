restoredefaultpath
clear all; clc;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/';
%pn.plstoolbox   = [pn.root, 'D2_PLS_VarTbx/T_tools/PLS_LNDG2018/Pls/']; addpath(genpath(pn.plstoolbox));
pn.plstoolbox   = [pn.root, 'B4_PLS_preproc2/T_tools/pls/']; addpath(genpath(pn.plstoolbox));
pn.plsdir       = [pn.root, 'D2_PLS_VarTbx/B_data/A_PLSdir/'];

cd(pn.plsdir);

% run the below to calculate the model

% batch_plsgui('meancentPLS_STSWD_VarTbx_v1_N93_byAge_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('meancentPLS_STSWD_VarTbx_v1_YAonly_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('meancentPLS_STSWD_VarTbx_v1_OAonly_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('behavPLS_STSWD_VarTbx_DDM1234_4min1_v1_N93_3mm_1000P1000B_BfMRIanalysis.txt')
% batch_plsgui('behavPLS_fullModel_STSWD_YA_N42_3mm_1000P10.txt')

batch_plsgui('behavPLS_STSWD_VarTbx_YA_OA_2Group_v3_3mm_1000P1000B_BfMRIanalysis.txt')

% assess model results

plsgui
