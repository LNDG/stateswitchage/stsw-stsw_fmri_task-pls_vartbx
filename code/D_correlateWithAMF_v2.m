%% Plot brainscores +- CI outside PLS

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/';
pn.functions    = [pn.root, 'T_tools/']; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]);

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D2_PLS_VarTbx/B_data/A_PLSdir/meancentPLS_STSWD_VarTbx_v1_YAonly_3mm_1000P1000B_BfMRIresult.mat');

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = []; u2Data = [];
for indGroup = 1:1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
        u2Data{indGroup}(indCond,:) = result.boot_result.usc2(targetEntries,1);
    end
end

individualBrainScores = uData{1}';

individualBrainScores = -1.*individualBrainScores;

X = [1 1; 1 2; 1 3; 1 4];
b=X\individualBrainScores';
individualBrainScores_slope(:,1) = b(2,:);

%% get summary data

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

idx_AttFactor = find(ismember(STSWD_summary.IDs, IDs));
idx_BS = find(ismember(IDs,STSWD_summary.IDs));

%% plot relations

h = figure('units','normalized','position',[.1 .1 .3 .2]);
x = individualBrainScores_slope(idx_BS);
% add AMF
ax{1} = subplot(2,3,1); hold on;
    y = STSWD_summary.EEG_LV1.slope(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x,y)
    ylabel({'spectral BS';'(linear modulation)'})
% add drift
ax{2} = subplot(2,3,2); hold on;
    y = STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x,y)
    ylabel({'Drift Load 1'})
% add drift change
ax{3} = subplot(2,3,3); hold on;
    y = STSWD_summary.HDDM_vt.driftMRI_linear(idx_AttFactor,1);
    scatter(x,y, 'filled', 'MarkerFaceColor', [.5 1 1]);
    [r, p] = corrcoef(x,y)
    ylabel({'Drift';'(linear modulation)'})
% add 1/f
ax{4} = subplot(2,3,4); hold on;
    y = STSWD_summary.OneFslope.linear_win(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x,y)
    ylabel({'1/f slope';'(linear modulation)'})
% add entropy
ax{5} = subplot(2,3,5); hold on;
    y = STSWD_summary.SE.data_slope_win(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x, y)
    ylabel({'entropy';'(linear modulation)'})
% add pupildiff
ax{6} = subplot(2,3,6); hold on;
    y = STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x, y)
    ylabel({'pupil';'(linear modulation)'})
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

% make background black, axes labels white
set(h,'Color','k')
for indAx = 1:6; ax{indAx}.Color = [0 0 0]; ax{indAx}.XColor = [1 1 1]; ...
        ax{indAx}.YColor = [1 1 1]; end


%% correlate with thalamic score

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4_stBL/behavPLS_ER_fullModel_win_STSWD_YA_N42_3mm_1000P10_fMRIresult_bs.mat'], 'BS')

idx_AttFactor = find(ismember(BS.IDs, IDs));
idx_BS = find(ismember(IDs,BS.IDs));


h = figure('units','normalized','position',[.1 .1 .3 .2]);
x = individualBrainScores_slope(idx_BS);
% add AMF
ax{1} = subplot(1,2,1); cla; hold on;
    x = individualBrainScores_slope(idx_BS);
    y = BS.behav(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [.5 1 1]);
    [r, p] = corrcoef(x,y)
    ylabel({'behavPLS: behavioral score';'(linear modulation)'})
    xlabel({'meanPLS: BOLD entropy';'(linear modulation)'})
ax{2} = subplot(1,2,2); cla; hold on;
    x = individualBrainScores_slope(idx_BS);
    y = BS.data(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x,y)
    ylabel({'behavPLS: brainscore';'(linear modulation)'})
    xlabel({'meanPLS: BOLD entropy';'(linear modulation)'})
    
% make background black, axes labels white
set(h,'Color','k')
for indAx = 1:2; ax{indAx}.Color = [0 0 0]; ax{indAx}.XColor = [1 1 1]; ...
        ax{indAx}.YColor = [1 1 1]; end

set(findall(gcf,'-property','FontSize'),'FontSize',16)
