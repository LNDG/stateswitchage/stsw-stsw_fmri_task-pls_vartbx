restoredefaultpath
clear all; clc;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/';
pn.plstoolbox   = [pn.root, 'B4_PLS_preproc2/T_tools/pls/']; addpath(genpath(pn.plstoolbox));
pn.plsdir       = [pn.root, 'D2_PLS_VarTbx/B_data/A_PLSdir/'];

cd(pn.plsdir);

% run the below to calculate the model

batch_plsgui('meancentPLS_STSWD_loads12_VarTbx_v6_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_loads12_VarTbx_v7_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_loads12_VarTbx_v1_YAOAgroups_no1228_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_loads1234_VarTbx_v7_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_loads234_VarTbx_v7_3mm_1000P1000B_BfMRIanalysis.txt')

batch_plsgui('behavPLS_STSWD_VarTbx_YA_OA_2Group_v7_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_load1_VarTbx_YA_OA_2Group_v7_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_1234Loadwise_VarTbx_YA_OA_2Group_v7_3mm_1000P1000B_BfMRIanalysis.txt')

batch_plsgui('meancentPLS_STSWD_loads1234_Slopes_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_Slopes_YA_OA_2Group_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_Slopes_YA_OA_1Group_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_Slopes1234_YA_OA_2Group_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_Slopes1234_YA_OA_1Group_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_4min3min2_Slopes_YA_OA_2Group_v7_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_1234Loadwise_Slopes_YA_OA_2Group_v7_3mm_1000P1000B_BfMRIanalysis.txt')

% assess model results

plsgui
