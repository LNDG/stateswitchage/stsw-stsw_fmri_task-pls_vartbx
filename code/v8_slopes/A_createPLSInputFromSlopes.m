% create PLS input from Varibility toolbox matrices

% copy original PLS mean matrices
% add in information from Variability Toolbox

% 190129 | created by JQK

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D2_PLS_VarTbx/';
pn.VarTBXfiles = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/L_V5motion/B_data/I_spectralPower/';
pn.origFiles = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SD_STSWD_task_v2/';
pn.maskFile = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/VoxelOverlap/'];
pn.outPath = [pn.root, 'B_data/A_PLSdir/'];
pn.tools = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/T_tools/']; addpath(genpath(pn.tools));

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc

for indID = 1:numel(IDs)
    try
    disp(['Processing subject ', IDs{indID}, '.']);

    % load subject's sessiondata file
    a = load([pn.origFiles, 'task_', IDs{indID}, '_BfMRIsessiondata.mat']);

    % load common coordinates
    load([pn.maskFile, 'coords_N95.mat'], 'final_coords_withoutZero');
    final_coords = final_coords_withoutZero;
    
    conditions=a.session_info.condition(1:numConds_raw);
    a = rmfield(a,'st_datamat');
    a = rmfield(a,'st_coords');

    %replace fields with correct info.
    a.session_info.datamat_prefix   = (['SD_', IDs{indID}, '_Slopes']); % SD PLS file name; _Bfmirsessiondata will be automatically appended!
    a.st_coords = final_coords;     % constrain analysis to shared non-zero GM voxels
    a.pls_data_path = pn.outPath;

    % load subject values for corresponding coordinates
    for indCond = 1:numConds_raw
        fname = [pn.VarTBXfiles,IDs{indID},'_Slopes_L',num2str(indCond),'.nii'];
        img = double(S_load_nii_2d(fname)); clear fname;
        img = img(final_coords,:); % restrict to final_coords
        a.st_datamat(indCond,:) = img;
        a.st_datamat(isnan(a.st_datamat)) = 0;
        clear img;
    end
    a.st_evt_list = a.st_evt_list(1:size(a.st_datamat,1));
    a.num_subj_cond = a.num_subj_cond(1:size(a.st_datamat,1));
    
    a.session_info.num_conditions = size(a.st_datamat,1);
    a.session_info.condition = a.session_info.condition(1:size(a.st_datamat,1));
    a.session_info.condition_baseline = a.session_info.condition_baseline(1:size(a.st_datamat,1));
    a.session_info.num_conditions0 = size(a.st_datamat,1);
    a.session_info.condition0 = a.session_info.condition0(1:size(a.st_datamat,1));
    a.session_info.condition_baseline0 = a.session_info.condition_baseline0(1:size(a.st_datamat,1));
    a.session_info.run = 1:numConds_raw;
    
    % linear 1234 regression
    for indVox = 1:size(a.st_datamat,2)
       X = [1 1; 1 2; 1 3; 1 4];
       b = regress(a.st_datamat(1:4, indVox), X);
       a.st_datamat(5,indVox) = b(2);
    end
    
    % average of loads 1 to 4
    a.st_datamat(6,:) = squeeze(nanmean(a.st_datamat(1:4,:),1));
    
    a.session_info.condition(5,1) = {'SDlinear1234'};
    a.session_info.condition(6,1) = {'Slope1234Mean'};
    
    a.st_datamat(7,:) = a.st_datamat(2,:)-a.st_datamat(1,:);
    a.st_datamat(8,:) = a.st_datamat(3,:)-a.st_datamat(2,:);
    a.st_datamat(9,:) = a.st_datamat(4,:)-a.st_datamat(3,:);
    
    a.session_info.condition(7,1) = {'L2minL1'};
    a.session_info.condition(8,1) = {'L3minL2'};
    a.session_info.condition(9,1) = {'L4minL3'};
    
    a.session_info.condition0 = a.session_info.condition;

    %% edit condition counters
    a.session_info.num_conditions  = numel(a.session_info.condition);
    a.session_info.num_conditions0 = numel(a.session_info.condition);

    %% add zero vector to st_datamat if condition(=session) not available
    % update condition variables according to count of conditions

    for indCond = size(a.session_info.condition_baseline,2)+1:numel(a.session_info.condition)

       a.session_info.condition_baseline{indCond}  = a.session_info.condition_baseline{1};
       a.session_info.condition_baseline0{indCond} = a.session_info.condition_baseline0{1};
    end

    % num_subj_cond AND st_evt_list: ones or numbers = count of conditions
    a.num_subj_cond = repmat(1,1,numel(a.session_info.condition));
    a.st_evt_list   = 1:1:numel(a.session_info.condition);

    
    save([pn.outPath, a.session_info.datamat_prefix ,'_BfMRIsessiondata.mat'],'-struct','a','-mat');
    disp ([IDs{indID} ' done!'])
    catch
    end
end