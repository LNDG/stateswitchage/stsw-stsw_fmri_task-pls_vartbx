%% Plot brainscores +- CI outside PLS

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data//mri/task/analyses/D_PLS_Dim/';
pn.functions    = [pn.root, 'T_tools/']; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]);

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools/')
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools/RainCloudPlots'))

% removed 2258 for now, as processing has not yet finished
IDs_YA = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};
IDs_OA = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2261'};

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D2_PLS_VarTbx/B_data/A_PLSdir/';
load([pn.data, 'meancentPLS_STSWD_loads1234_Slopes_3mm_1000P1000B_BfMRIresult.mat'])
% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:numel(groupsizes)
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1); % vsc: designscore
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1); % usc: brainscore
    end
end

%% plot RainCloudPlot
% 
% h = figure('units','normalized','position',[.1 .1 .15 .2]);
% for indGroup = 1:2
%     %subplot(1,2,indGroup)
%     set(gcf,'renderer','Painters')
%     curData = uData{indGroup}';
% 
%      % read into cell array of the appropriate dimensions
%         data = []; data_ws = [];
%         for i = 1:4
%             for j = 1:1
%                 data{i, j} = squeeze(curData(:,i));
%                 % individually demean for within-subject visualization
%                 data_ws{i, j} = curData(:,i)-...
%                     nanmean(curData(:,:),2)+...
%                     repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
%             end
%         end
% 
%         % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!
% 
%         if indGroup ==1
%             cl = 2.*[.3 .1 .1];
%         elseif indGroup ==2
%             cl = 2.*[.1 .1 .3];
%         end
% 
%         box off
%         hold on;
%             h_rc = rm_raincloud(data, cl,1);
%             view([90 -90]);
%             axis ij
%         box(gca,'off')
%         %set(gca, 'YTick', [1,2,3,4]);
%         set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
%         ylabel('Target load'); xlabel({'Brainscore (a.u.)'})
%         %title('1/f slope modulation'); 
%         set(findall(gcf,'-property','FontSize'),'FontSize',20)
%         %xlim([-30000 50000]); 
%         curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
% end


pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary_YAOA.mat'], 'STSWD_summary')

figure;
    hold on;
    idx_sumID = ismember(STSWD_summary.IDs, IDs_YA);
    curDrift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_sumID,1:4),2));
    [sortVal, sortIdx] = sort(curDrift,  'descend');
    l1 = plot(squeeze(nanmean(uData{1}(1:4, sortIdx(1:floor(numel(sortIdx)/2))),2)), 'k', 'LineWidth', 2);
    l2 = plot(squeeze(nanmean(uData{1}(1:4, sortIdx(floor(numel(sortIdx)/2)+1:end)),2)), 'k--', 'LineWidth', 2);
    idx_sumID = ismember(STSWD_summary.IDs, IDs_OA);
    curDrift = squeeze(nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_sumID,1:4),2));
    [sortVal, sortIdx] = sort(curDrift,  'descend');
    l3 = plot(squeeze(nanmean(uData{2}(1:4, sortIdx(1:floor(numel(sortIdx)/2))),2)), 'r', 'LineWidth', 2);
    l4 = plot(squeeze(nanmean(uData{2}(1:4, sortIdx(floor(numel(sortIdx)/2)+1:end)),2)), 'r--', 'LineWidth', 2);
    legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
    legend('boxoff')
    xlabel('Load')
    ylabel('Brainscore')
set(findall(gcf,'-property','FontSize'),'FontSize',16)


figure;
    hold on;
    idx_sumID = ismember(STSWD_summary.IDs, IDs_YA);
    curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
    [sortVal, sortIdx] = sort(curDrift,  'descend');
    l1 = plot(squeeze(nanmean(uData{1}(1:4, sortIdx(1:floor(numel(sortIdx)/2))),2)), 'k', 'LineWidth', 2);
    l2 = plot(squeeze(nanmean(uData{1}(1:4, sortIdx(floor(numel(sortIdx)/2)+1:end)),2)), 'k--', 'LineWidth', 2);
    idx_sumID = ismember(STSWD_summary.IDs, IDs_OA);
    curDrift = squeeze(nanmean(STSWD_summary.OneFslope.data(idx_sumID,1:4),2));
    [sortVal, sortIdx] = sort(curDrift,  'descend');
    l3 = plot(squeeze(nanmean(uData{2}(1:4, sortIdx(1:floor(numel(sortIdx)/2))),2)), 'r', 'LineWidth', 2);
    l4 = plot(squeeze(nanmean(uData{2}(1:4, sortIdx(floor(numel(sortIdx)/2)+1:end)),2)), 'r--', 'LineWidth', 2);
    legend([l1, l2, l3], {'High YA'; 'Low YA'; 'OA'})
    legend('boxoff')
    xlabel('Load')
    ylabel('Brainscore')
set(findall(gcf,'-property','FontSize'),'FontSize',16)
